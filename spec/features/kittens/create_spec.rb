require 'rails_helper'

feature "Kitten", :type => :feature do
  scenario "Create a new kitten" do
    visit "/kittens/new"

    fill_in "kitten_name", :with => "SuperCat"
    fill_in "kitten_age", :with => "200"
    fill_in "kitten_softness", :with => "10"
    fill_in "kitten_cuteness", :with => "10"

    click_button "Create kitten!"

    expect(page).to have_text("SuperCat")
    expect(page).to have_text("200")
    expect(page).to have_text("10")
  end

  scenario "Get kitten JSON-information" do
    kitten = create(:kitten)
    visit "api/kitten/#{kitten.id}"
    expect(page).to have_content(kitten.to_json(only: [:id, :name, :age, :softness, :cuteness]))
  end

  scenario "Get kittens JSON-information" do
    kittens = create_list(:kitten, 20)
    visit "api/kittens"
    expect(page).to have_content(kittens.to_json(only: [:id, :name, :age, :softness, :cuteness]))
  end
end
