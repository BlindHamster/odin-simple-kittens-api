require 'rails_helper'

describe Kitten, type: :model do
  it "has a valid factory" do
    kitten = build(:kitten)
    expect(kitten).to be_valid
  end
end

describe Kitten do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
    it { is_expected.to validate_length_of(:name) }
    it { is_expected.to validate_presence_of(:age) }
    it { is_expected.to validate_presence_of(:softness) }
    it { is_expected.to validate_presence_of(:cuteness) }
end
