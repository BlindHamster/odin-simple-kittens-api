FactoryGirl.define do
  factory :kitten do
    name     { get_name }
    age      { rand(1..20) }
    cuteness { rand(1..10) }
    softness { rand(1..10) }
  end
end

def get_name
  begin
    name = Faker::Pokemon.name + "#{rand(1..1000)}"
  end while name.size < 5
  name
end
