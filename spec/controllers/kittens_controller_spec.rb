require 'rails_helper'

describe KittensController, type: :controller do

  describe "POST #create" do
    context "with valid attributes" do
      it "creates new kitten" do
        post :create, params: { kitten: attributes_for(:kitten) }
        expect(Kitten.count).to eq(1)
      end
    end

    context "with invalid attributes" do
      it "does not create new kitten" do
        post :create, params: { kitten: attributes_for(:kitten, name: "") }
        expect(Kitten.count).to eq(0)
      end
    end
  end
end
