# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

names = []
50.times do
  names << Faker::Pokemon.name
end
names.uniq

names.each_with_index do |name, index|
  age = rand(1..20)
  softness = rand(1..10)
  cuteness = rand(1..10)
  Kitten.create(name: name,
                age: age,
                softness: softness,
                cuteness: cuteness)
  puts "#{index +1 }/50 created!"
end
