Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'kittens#index'
  resources :kittens

  namespace :api, defaults: { format: :json } do
    get '/kittens', to: 'kittens#index'
    get '/kitten/:id', to: 'kittens#show'
  end
end
