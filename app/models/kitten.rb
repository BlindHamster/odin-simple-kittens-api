class Kitten < ApplicationRecord
  validates :name, presence: true, uniqueness: true, length: { minimum: 5 }
  validates :age, presence: true
  validates :softness, presence: true
  validates :cuteness, presence: true
end
